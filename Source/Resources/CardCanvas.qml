import QtQuick 2.10
import QtQuick.Layouts 1.10
import QtQuick.Controls 2.13
import org.kde.kirigami 2.13 as Kirigami

Kirigami.ScrollablePage {
    globalToolBarStyle: Kirigami.ApplicationHeaderStyle.None
    actions {
        main: Kirigami.Action {
            text: "Create New Note"
            iconName: "list-add"
            visible: app.currentModel !== null
            onTriggered: {
                app.currentModel.createStaging()
                sheet.open()
            }
        }
    }

    Kirigami.OverlaySheet {
        id: sheet
        header: RowLayout {
            Kirigami.Heading {
                text: "Create New Note..."
            }
            Item { Layout.fillWidth: true }
            Button {
                text: "Create"
                icon.name: "list-add"
                onClicked: {
                    app.currentModel.staging["title"] = title.text
                    app.currentModel.staging["contents"] = contents.text
                    app.currentModel.commitStaging()
                    sheet.close()
                }
            }
        }
        contentItem: ColumnLayout {
            TextField {
                id: title
            }
            TextArea {
                id: contents

                Layout.fillWidth: true
            }
        }
    }

    Timer {
        running: true
        interval: 500
        repeat: true
        onTriggered: {
            if (app.currentModel !== null) {
                print(`${app.currentModel.rowCount()}`)
            }
        }
    }

    Kirigami.CardsListView {
        model: app.currentModel ?? 0
        delegate: Kirigami.Card {
            visible: !model["Note-object"]["pendingDelete"]
            contentItem: ColumnLayout {
                RowLayout {
                    TextField {
                        id: title
                        text: model["title"]
                    }
                    Button {
                        text: "Delete"
                        onClicked: model["Note-object"].stageDelete()
                    }
                }
                TextArea {
                    id: contents
                    text: model["contents"]

                    Layout.fillWidth: true
                }
                Component.onCompleted: print(`${model["title"]}`)
            }
        }
    }
}
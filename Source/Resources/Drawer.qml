import QtQuick 2.10
import QtQuick.Layouts 1.10
import QtQuick.Controls 2.12
import org.kde.kirigami 2.13 as Kirigami

Kirigami.GlobalDrawer {
    modal: false
    handleVisible: false
    drawerOpen: true

    header: Kirigami.AbstractApplicationHeader {
        RowLayout {
            anchors.fill: parent
            anchors.leftMargin: Kirigami.Units.smallSpacing
            anchors.rightMargin: Kirigami.Units.smallSpacing

            Kirigami.SearchField {
                Layout.fillWidth: true
            }
            Button {
                icon.name: "list-add"
                text: "Create New Group"
                onClicked: DataModel.createStaging()
            }
        }
    }

    Kirigami.Card {
        visible: DataModel.staging !== null
        Layout.alignment: Qt.AlignTop

        contentItem: TextField {
            onEditingFinished: {
                DataModel.staging["name"] = text
                DataModel.commitStaging()
            }
        }
    }
    Repeater {
        Layout.alignment: Qt.AlignTop

        model: DataModel
        delegate: Kirigami.Card {
            banner.title: model["name"]
            showClickFeedback: true
            onClicked: app.currentModel = model["children-Note"]
        }
    }
    Item { Layout.fillHeight: true }
}
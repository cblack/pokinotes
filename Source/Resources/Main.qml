import QtQuick 2.10
import QtQuick.Layouts 1.10
import org.kde.kirigami 2.13 as Kirigami

Kirigami.ApplicationWindow {
    id: app
    property var currentModel: null

    globalDrawer: Drawer {}
    pageStack.initialPage: CardCanvas {}
}
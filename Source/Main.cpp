#include <QApplication>
#include <QCoreApplication>
#include <QDebug>
#include <QIcon>
#include <QModelIndex>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QScopedPointer>
#include <KLocalizedContext>

#include "PokiNotes.gen.h"

auto main(int argc, char *argv[]) -> int {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    auto app = QScopedPointer(new QApplication(argc, argv));

    Note::prepareDatabase();
    Tab::prepareDatabase();

    auto model = QScopedPointer(new TabModel);

    QApplication::setOrganizationName("KDE");
    QApplication::setOrganizationDomain("org.kde");
    QApplication::setApplicationName("PokiNotes");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.rootContext()->setContextProperty("DataModel", model.data());
    const QUrl url(QStringLiteral("qrc:/Main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     app.data(), [url](QObject *obj, const QUrl &objUrl) {
        if ((obj == nullptr) && url == objUrl) {
            QCoreApplication::exit(-1);
        }
    }, Qt::QueuedConnection);
    engine.load(url);

    return QApplication::exec();
}